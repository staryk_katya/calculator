﻿using Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public static class Calcution
    {
        public static double FirstNumber { get; set; }
        public static double SecondNumber { get; set; }
        public static OperationList Operation { get; set; }

        public static double Calculation()
        {
            double result = 0;
            switch (Operation)
            {
                case OperationList.Addition:
                    result = FirstNumber + SecondNumber;
                    break;
                case OperationList.Division:
                    result = FirstNumber / SecondNumber;
                    break;
                case OperationList.Multiplication:
                    result = FirstNumber * SecondNumber;
                    break;
                case OperationList.Submition:
                    result = FirstNumber - SecondNumber;
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
