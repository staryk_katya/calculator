﻿using Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public static class ConsoleInterface
    {
        public static void Welcome()
        {
            Console.WriteLine("This is my first calculator.");
        }

        public static void GetNumber(int number)
        {
            switch (number)
            {
                case 1:
                    Console.WriteLine("Enter first number");
                    Calcution.FirstNumber = Convert.ToDouble(Console.ReadLine());
                    break;
                case 2:
                    Console.WriteLine("Enter second number");
                    Calcution.SecondNumber = Convert.ToDouble(Console.ReadLine());
                    break;
                default:
                    break;
            }
        }

        public static void GetOperation()
        {
            bool isValid = false;

            while (!isValid)
            {
                Console.Clear();
                Console.WriteLine("Choose mathematic operation");
                Console.WriteLine("1) Addition (+);");
                Console.WriteLine("2) Submition (-);");
                Console.WriteLine("3) Multiplication (*);");
                Console.WriteLine("4) Division (/);");
                var answer = Console.ReadLine();
                switch (answer)
                {
                    case "1":
                    case "+":
                        isValid = true;
                        Calcution.Operation = OperationList.Addition;
                        break;
                    case "2":
                    case "-":
                        isValid = true;
                        Calcution.Operation = OperationList.Submition;
                        break;
                    case "3":
                    case "*":
                        isValid = true;
                        Calcution.Operation = OperationList.Multiplication;
                        break;
                    case "4":
                    case "/":
                        Calcution.Operation = OperationList.Division;
                        if (Calcution.SecondNumber == 0)
                        {
                            Console.WriteLine("Cannot division by ZERO!!!");
                            GetNumber(2);
                        }
                        isValid = true;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Please enter correct data");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
            }
        }

        public static void ShowResult(double result)
        {
            Console.WriteLine("Your result is: " + result);
        }
    }
}
